package me.Alwiniasty.QuickShopBAW;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.plugin.Plugin;
import org.maxgamer.quickshop.Event.ShopCreateEvent;
import org.maxgamer.quickshop.Event.ShopPriceChangedEvent;
import org.maxgamer.quickshop.QuickShop;


public class CreateShopEvent implements Listener {

    private QuickShop qsPlugin = QuickShop.instance;
    private Plugin plugin = Main.instance;

    @EventHandler
    public void onShopCreateEvent(ShopCreateEvent event) {

        if (!BAW.isPriceOk(event.getShop().getPrice(), QuickShopUtils.getSameShops(event.getShop()))) {
            event.getPlayer().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("message-cannot-create")));
            event.setCancelled(true);
        }

    }

    @EventHandler
    public void onShopPriceChange(ShopPriceChangedEvent event) {

        Bukkit.broadcastMessage("Test");

        if (!BAW.isPriceOk(event.getNewPrice(), QuickShopUtils.getSameShops(event.getShop()))) {
//            No event player :/ and this chan be confusing when staff change price
//            event.getShop().getOwner().sendMessage(ChatColor.translateAlternateColorCodes('&', plugin.getConfig().getString("message-cannot-create")));
            event.getShop().setPrice(event.getOldPrice());
            event.getShop().update();
        }
    }
}


