package me.Alwiniasty.QuickShopBAW;

import org.bukkit.entity.Player;
import org.maxgamer.quickshop.Shop.Shop;

import java.util.List;

public class BAW {

    public static boolean isPriceOk(double price, List<Shop> shopList) {


        if (shopList.size() <= 3) {
            return true;
        }


        shopList.sort((o1, o2) -> { return (int) (o1.getPrice() - o2.getPrice());} );
        double q1;
        double q2;
        double q3;


        if (price >= shopList.get(0).getPrice() && price <= shopList.get(shopList.size()-1).getPrice()) {
            return true;
        }


        int middle = shopList.size()/2;

        if (middle%2 != 0) {
            q2 = shopList.get(middle).getPrice();
        } else {
            q2 = (shopList.get(middle).getPrice()+shopList.get(middle+1).getPrice())/2;
        }


        double halfMiddle = middle/2;

        if (halfMiddle%2 != 0) {
            q1 = shopList.get((int) halfMiddle).getPrice();
            q3 = shopList.get((int) halfMiddle+middle).getPrice();
        } else {
            q1 = (shopList.get((int) halfMiddle+1).getPrice()+shopList.get((int) halfMiddle).getPrice())/2;
            q3 = (shopList.get((int) halfMiddle+1+middle).getPrice()+shopList.get((int) halfMiddle+middle).getPrice())/2;
        }


        double iqr = 1.5 * (q3-q1);


        if (price >= q1 - iqr && price <= q3 + iqr) {
            return true;
        }


        return false;
    }

}
