package me.Alwiniasty.QuickShopBAW;

import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.util.BlockIterator;
import org.maxgamer.quickshop.QuickShop;
import org.maxgamer.quickshop.Shop.Shop;
import org.maxgamer.quickshop.Util.ItemMatcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class QuickShopUtils {

    private static QuickShop qsPlugin = QuickShop.instance;

    public static List<Shop> getSameShops(Shop shop) {
        String world = shop.getLocation().getWorld().getName();

        List<Shop> shops = new ArrayList<Shop>();


        for (HashMap<Location, Shop> shopLocation : qsPlugin.getShopManager().getShops(world).values()) {
            for (Shop s : shopLocation.values()) {
                if (s.getShopType() == shop.getShopType()) {

                    if (s.getItem().isSimilar(shop.getItem())) {
                        shops.add(s);
                    }
                }
            }
        }

        return shops;
    }

    public static Shop getShopLookingAt(Player player) {
        BlockIterator blt = new BlockIterator(player, 10);
        if (!blt.hasNext()) {
            return null;
        }
        while (blt.hasNext()) {
            Block b = blt.next();
            Shop shop = qsPlugin.getShopManager().getShop(b.getLocation());
            if (shop != null && (shop.getModerator().isModerator(player.getUniqueId()))) {
                return shop;
            }
        }
        return null;
    }

}
