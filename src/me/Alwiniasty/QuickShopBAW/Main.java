package me.Alwiniasty.QuickShopBAW;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;
import org.maxgamer.quickshop.Shop.Shop;

import java.util.List;

public class Main extends JavaPlugin {

    public static Plugin instance;

    @Override
    public void onEnable() {
        this.saveDefaultConfig();
        PluginManager pm = getServer().getPluginManager();
        pm.registerEvents(new CreateShopEvent(), this);
        super.onEnable();
    }

    @Override
    public void onLoad() {
        instance = this;
    }

    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender.hasPermission("QuickShopBAW.admin")) {
            reloadConfig();
            sender.sendMessage(ChatColor.GREEN+"Reloaded!!!");
            return true;
        }
        return false;
    }

}
